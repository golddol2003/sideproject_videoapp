//
//  AppDelegate.swift
//  SideProject
//
//  Created by 이민철 on 2021/02/01.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		guard let window = window else{
			return true
		}
		let coordinator = SceneCoordinator(window: window)
		let splashVM = MainVM(title: "main", sceneCoordinator: coordinator)
		let mainScene = Scene.main(splashVM)
		
		coordinator.transition(to: mainScene, using: .root, animated: false)
		return true
	}

}

