//
//  Scene.swift
//  SideProject
//
//  Created by 이민철 on 2021/02/01.
//

import Foundation
import UIKit
enum Scene {
	//name(viewModel)
	case main(MainVM)
}


extension Scene {
	func instantiate(from storyboard: String = "Main") -> UIViewController {
		let storyboard = UIStoryboard.init(name: storyboard, bundle: nil)
		
		switch self {
		//메인
		case .main(let viewModel):
			guard let nav = storyboard.instantiateViewController(withIdentifier: "NavMain") as? UINavigationController else{
				fatalError()
			}
			guard  var mainVC = nav.viewControllers.first as? MainVC else {
				fatalError()
				
			}
			
			mainVC.bind(viewModel: viewModel)
			
			return nav
		}
	}
}
