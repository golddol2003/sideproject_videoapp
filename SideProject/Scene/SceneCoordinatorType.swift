//
//  SceneCordinatorType.swift
//  coconut_ios
//
//  Created by 이민철 on 2020/12/29.
//

import Foundation
import RxSwift
protocol SceneCoordinatorType {

	@discardableResult
	func transition(from storyboard: String , to scene: Scene, using style: TransitionStyle, animated: Bool) -> Completable

	
	@discardableResult
	 func close(animated: Bool) -> Completable
	

	
}
