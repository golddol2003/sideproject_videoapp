//
//  TransitionModel.swift
//  coconut_ios
//
//  Created by 이민철 on 2020/12/29.
//

import Foundation

enum TransitionStyle {
	case root
	case push
	case pop
	case modal
}

enum TrasitionError: Error {
	case navigationControllerMissing
	case cannotPop
	case unknown
}
