//
//  SceneCoordinator.swift
//  coconut_ios
//
//  Created by 이민철 on 2020/12/29.
//

import Foundation
import RxSwift
import RxCocoa
import NSObject_Rx
import UIKit

extension UIViewController {
	
	var sceneViewController: UIViewController {
		print(self.children.first ?? self)
		return self.children.first ?? self
	}
	
}

class SceneCoordinator: SceneCoordinatorType {
	

	
	private let bag = DisposeBag()
	private var window : UIWindow
	private var currentVC: UIViewController
	
	required init(window: UIWindow){
		self.window = window
		currentVC = window.rootViewController!
	}
	@discardableResult
	func transition(from stortboard: String = "Main", to scene: Scene, using style: TransitionStyle, animated: Bool) -> Completable {
		let subject = PublishSubject<Void>()
		
		let target = scene.instantiate(from: stortboard)
		
		switch style {
		case .root :
			currentVC = target.sceneViewController
			window.rootViewController = target
			subject.onCompleted()
			
		case .push :
			guard let nav = currentVC.navigationController  else {
				subject.onError(TrasitionError.navigationControllerMissing)
				break
			}
			nav.rx.willShow
				.subscribe(onNext: {[unowned self] event in
					self.currentVC = event.viewController.sceneViewController
				}).disposed(by: bag)
			currentVC.navigationController?.pushViewController(target, animated: animated)
//			nav.navigationController?.pushViewController(target, animated: animated)
			currentVC = target.sceneViewController
			
			subject.onCompleted()
			
		case .modal :
			target.modalTransitionStyle = .crossDissolve
			target.modalPresentationStyle = . overCurrentContext
			currentVC.present(target, animated: animated){
				subject.onCompleted()
			}
			currentVC = target.sceneViewController
		case .pop :
			guard let nav = currentVC.navigationController else {
				subject.onError(TrasitionError.navigationControllerMissing)
				break
			}
			nav.rx.willShow
				.subscribe(onNext: { [unowned self] event in
					self.currentVC = event.viewController.sceneViewController
				}).disposed(by: bag)
			
			nav.navigationController?.popViewController(animated: true)
		}
			return subject.ignoreElements()
	 }
	
	func close(animated: Bool) -> Completable {
		let subject = PublishSubject<Void>()
		
		if let presentingVC = self.currentVC.presentingViewController{
			
			self.currentVC.dismiss(animated: animated){
				self.currentVC = presentingVC.sceneViewController
				subject.onCompleted()
			}
		}else if let nav = self.currentVC.navigationController {
			guard nav.popViewController(animated: animated) != nil else {
				subject.onError(TrasitionError.cannotPop)
				return subject.ignoreElements()
			}
			self.currentVC = nav.viewControllers.last!
			subject.onCompleted()
		}else{
			subject.onError(TrasitionError.unknown)
		}
		return subject.ignoreElements()
	}
}
