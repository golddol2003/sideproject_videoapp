//
//  CommonViewModel.swift
//  SideProject
//
//  Created by 이민철 on 2021/02/01.
//

import Foundation
import RxSwift
import RxCocoa
class CommonViewModel: NSObject {
	
	let title: Driver<String>?
	let sceneCoordinator: SceneCoordinatorType
	
	init(title: String, sceneCoordinator: SceneCoordinatorType){
		self.title = Observable.just(title).asDriver(onErrorJustReturn: "")
		self.sceneCoordinator = sceneCoordinator
	}
}
